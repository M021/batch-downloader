package com.mlcodehk.batchDownloader;

import com.mlcodehk.http.exception.HttpClientException;
import com.mlcodehk.http.service.DefaultHttpService;
import com.mlcodehk.http.service.HttpService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class Test {

    public static void main(String[] args) throws IOException, HttpClientException, InterruptedException {

        String baseUri = System.getProperty("user.dir");
        final String urlFilePath = baseUri + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "test.txt"; //add the urls which you want to download in this file.
        final String saveToDirectory = baseUri + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "output";

        final int numOfThread = 10;
        final int throttleMillisecond = 1000;

        HttpService httpService = new DefaultHttpService(); //you can extend the class and customize HTTP handling.
        //HttpService httpService = new DefaultHttpService(maxTotalConnection, maxPerRoute, keystorePath, password)
        //you could import the cert you trust to truststore.jks
        //classpath:/jks/truststore.jks
        //password: password


        BatchDownloadManager mgr = new BatchDownloadManager(httpService, numOfThread, throttleMillisecond, true);

        logger.info("Start batch download.");
        mgr.download(urlFilePath, saveToDirectory);
        logger.info("End batch download.");
    }
}

