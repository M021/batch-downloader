package com.mlcodehk.batchDownloader;

public class DownloadInfo {

    private String url;

    private String saveToFilePath;

    private DownloadStatus downloadStatus;

    public DownloadInfo(String url, String saveToFilePath, DownloadStatus downloadStatus) {
        this.url = url;
        this.saveToFilePath = saveToFilePath;
        this.downloadStatus = downloadStatus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSaveToFilePath() {
        return saveToFilePath;
    }

    public void setSaveToFilePath(String saveToFilePath) {
        this.saveToFilePath = saveToFilePath;
    }

    public DownloadStatus getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        this.downloadStatus = downloadStatus;
    }
}
