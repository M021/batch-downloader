package com.mlcodehk.batchDownloader;

public enum DownloadStatus {
    INIT, PROCESS, SUC, FAIL
}
