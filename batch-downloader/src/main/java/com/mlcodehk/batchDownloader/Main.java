package com.mlcodehk.batchDownloader;

import com.mlcodehk.http.exception.HttpClientException;
import com.mlcodehk.http.service.DefaultHttpService;
import com.mlcodehk.http.service.HttpService;
import lombok.extern.slf4j.Slf4j;

import java.io.*;

@Slf4j
public class Main {

    public static void main(String[] args) throws IOException, HttpClientException, InterruptedException, HttpClientException {

        if (args.length != 4) {
            System.out.println("[Error]: Incorrect command.");
            System.out.println("[Usage]: java -jar batchDownloader.jar <URL_FILE_PATH> <SAVE_TO_DIRECTORY> <NUMBER_OF_THREAD> <THROTTLE_MILLISECOND>");
            return;
        }
        final String urlFilePath = args[0];
        final String saveToDirectory = args[1];
        final int numOfThread = Integer.parseInt(args[2]);
        final int throttleMillisecond = Integer.parseInt(args[3]); //if the value <= 0, it means no throttle.

        if(numOfThread < 1){
            logger.error("NUMBER_OF_THREAD must > 0");
            return;
        }

        HttpService httpService = new DefaultHttpService(); //you can extend the class and customize HTTP handling.
        //HttpService httpService = new DefaultHttpService(maxTotalConnection, maxPerRoute, keystorePath, password)
        //you could import the cert you trust to truststore.jks
        //classpath:/jks/truststore.jks

        BatchDownloadManager mgr = new BatchDownloadManager(httpService, numOfThread, throttleMillisecond, false);

        logger.info("Start batch download.");
        mgr.download(urlFilePath, saveToDirectory);
        logger.info("End batch download.");
    }
}
