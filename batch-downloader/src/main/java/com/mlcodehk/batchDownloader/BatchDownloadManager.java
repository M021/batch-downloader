package com.mlcodehk.batchDownloader;


import com.mlcodehk.http.service.HttpService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class BatchDownloadManager {

    private HttpService httpService;

    private  ExecutorService executorService;

    private Long throttleMilliSecond;

    private Boolean isDynamicThrottle;

    public BatchDownloadManager(HttpService httpService, int numOfThread, long throttleMilliSecond, boolean isDynamicThrottle) {
        this.httpService = httpService;
        this.executorService = Executors.newFixedThreadPool(numOfThread);
        this.throttleMilliSecond = throttleMilliSecond;
        this.isDynamicThrottle = isDynamicThrottle;
    }

    public void download(final String urlFilePath, final String saveToDirectory) throws IOException, InterruptedException {
        download(urlFilePath, saveToDirectory, null);
    }

    public void download(final String urlFilePath, final String saveToDirectory, Map<String, String> httpHeaders) throws IOException, InterruptedException {

        List<DownloadInfo> downloadInfoList = new ArrayList<>();

        File urlFile = new File(urlFilePath);

        //The format from the url file.
        //If you want to change the filename, you may use the format in example 1
        //example 1: https://www.google.com.hk/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png,filename1.png
        //example 2: https://www.google.com.hk/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png

        try (BufferedReader reader = new BufferedReader(new FileReader(urlFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] info = line.split(",");
                String url = info[0];
                String filename;
                if (info.length < 2) {
                    filename = url.substring(url.lastIndexOf("/") + 1);
                } else {
                    filename = info[1];
                }
                DownloadInfo downloadInfo = new DownloadInfo(url,
                        saveToDirectory + File.separator + filename, DownloadStatus.INIT);
                downloadInfoList.add(downloadInfo);
                executorService.submit(new DownloadFile(downloadInfo, httpService, httpHeaders, throttleMilliSecond, isDynamicThrottle));
            }
            executorService.shutdown();

            while (!executorService.isTerminated()) {
                //do nothing
            }

        } finally {
            List<DownloadInfo> sucList = new ArrayList<>();
            List<DownloadInfo> failList = new ArrayList<>();
            for (DownloadInfo info : downloadInfoList) {
                if (DownloadStatus.SUC == info.getDownloadStatus()) {
                    sucList.add(info);
                } else {
                    failList.add(info);
                }
            }
            logger.info("[Downloaded item] size: " + sucList.size());
            for (DownloadInfo info : sucList) {
                logger.info( "\n" +"source: " + info.getUrl() + "\n" + "destination:" + info.getSaveToFilePath());
            }
            logger.info("[Failed item] size: " + failList.size());
            for (DownloadInfo info : failList) {
                logger.info( "\n" +"source: " + info.getUrl() + "\n" + "destination:" + info.getSaveToFilePath());
            }
        }

    }

    private static class DownloadFile implements Runnable {

        private DownloadInfo downloadInfo;

        private HttpService httpService;

        private Map<String, String> httpHeaders;

        private Long throttleMilliSecond;

        private Boolean isDynamicThrottle;

        public DownloadFile(DownloadInfo downloadInfo, HttpService httpService, Map<String, String> httpHeaders,Long throttleMilliSecond, Boolean isDynamicThrottle) {
            this.downloadInfo = downloadInfo;
            this.httpService = httpService;
            this.httpHeaders = httpHeaders;
            this.throttleMilliSecond = throttleMilliSecond;
            this.isDynamicThrottle = isDynamicThrottle;
        }

        @Override
        public void run() {
            logger.info("Start to download file - url: " + downloadInfo.getUrl());

            File saveTo = new File(downloadInfo.getSaveToFilePath());
            if(!saveTo.isDirectory()){
                saveTo.getParentFile().mkdirs();
            }

            throttle();

            downloadInfo.setDownloadStatus(DownloadStatus.PROCESS);
            try (CloseableHttpResponse response = httpService.executeGet(new URL(downloadInfo.getUrl()), httpHeaders)) {
                HttpEntity entity = response.getEntity();
                writeFile(entity);
                downloadInfo.setDownloadStatus(DownloadStatus.SUC);
            } catch (Throwable e) {
                logger.info("Failed to download file - url: " + downloadInfo.getUrl(),e);
                downloadInfo.setDownloadStatus(DownloadStatus.FAIL);
            }

            return;
        }

        private void throttle() {
            try {

                if (throttleMilliSecond != null && throttleMilliSecond > 0) {
                    if (isDynamicThrottle != null && isDynamicThrottle) {
                        Random rand = new Random();
                        Long dynamicValue = Math.abs((rand.nextInt() * 1000L) % throttleMilliSecond);
                        logger.debug("dynamicValue : " + dynamicValue);
                        Thread.sleep(dynamicValue);
                    } else {
                        Thread.sleep(throttleMilliSecond);
                    }
                }
            } catch (Exception ignore) {

            }
        }

        private void writeFile(HttpEntity entity) throws IOException {
            try (OutputStream os = new BufferedOutputStream(new FileOutputStream(new File(downloadInfo.getSaveToFilePath())))) {
                InputStream is = new BufferedInputStream(entity.getContent());
                byte[] buffer = new byte[8192];
                int len;
                while ((len = is.read(buffer)) != -1) {
                    os.write(buffer, 0, len);
                }
            }
        }
    }

}
