# Batch Downloader

This project is to download all files from specified URLs which are stored in an input file.

- [Description](#description)
- [Dependency](#dependency)
- [Usage](#usage)


## Description

Sometimes, you may want to download a large number of images or files. However, it is very time-consuming if you input the URL to a browser or a download manager one by one. Therefore, I developed this program to help download files. All the URLs are pasted in an input file so that my program can download automatically all the files by reading the input file.

## Dependency

I use the library developed from my previous project in this project.
https://gitlab.com/M021/http-service

## Usage

    java -jar batchDownloader.jar <URL_FILE_PATH> <SAVE_TO_DIRECTORY> <NUMBER_OF_THREAD> <THROTTLE_MILLISECOND>

  URL_FILE_PATH: It is the input file path. The format in in the file is shown below. It supports two types of format.

   For the first format, the format allows a user to specify the output filename. "," is used to separate the URL and specified output filename.

    https://www.google.com.hk/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png,filename.png


For the second format, if you only input the URL, it will use the text after the last "/" as the file name. In this case, the filename is googlelogo_color_272x92dp.png

    https://www.google.com.hk/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png


   SAVE_TO_DIRECTORY: The directory to store the output file

   NUMBER_OF_THREAD: It must be > 0.

   THROTTLE_MILLISECOND: If it is <= 0, it means no throttle.

   BatchDownloadManager also supports dynamic throttle. The objective is to hide the HTTP call pattern.

   You can also add your own truststore in resourse/jks. Then you may modify the code to accept the truststore. Below is an example.

        HttpService httpService = new DefaultHttpService(maxTotalConnection, maxPerRoute, keystorePath, password);
